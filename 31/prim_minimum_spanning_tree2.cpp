// STL implementation of Prim's algorithm for MST
#include <bits/stdc++.h>

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
char startingpoint = 'A';
int  incPriority   = 0;
int  fat           = 0;

using namespace std;
#define INF 0x3f3f3f3f

// iPair ==>  Integer Pair
vector<string>         Svec;
typedef pair<int, int> iPair;

// This class represents a directed graph using
// adjacency list representation
class Graph {
    int V; // No. of vertices

    // In a weighted graph, we need to store vertex
    // and weight pair for every edge
    list<pair<int, int>>* adj;

public:
    Graph(int V); // Constructor

    // function to add an edge to graph
    void addEdge(int u, int v, int w);

    // Print MST using Prim's algorithm
    void primMST();
};

bool stringCompare(const string& left, const string& right) {
    return (left[0] > right[0]);
}

bool stringCompare2(const string& left, const string& right) {
    return (left[1] < right[1]);
}
bool stringCompare3(const string& left, const string& right) {
    return (left[2] > right[2]);
}
// Allocates memory for adjacency list
Graph::Graph(int V) {
    this->V = V;
    adj     = new list<iPair>[V];
}

void Graph::addEdge(int u, int v, int w) {
    if (startingpoint != 'A') {
        if ((char)u == 'A')
            u = startingpoint;
        else if ((char)u == startingpoint)
            u = 'A';
        if ((char)v == 'A')
            v = startingpoint;
        else if ((char)v == startingpoint)
            v = 'A';
    }
    u = (int)u - 65;
    v = (int)v - 65;
    adj[u].push_back(make_pair(v, w));
    adj[v].push_back(make_pair(u, w));
}

// Prints shortest paths from src to all other vertices
void Graph::primMST() {
    // Create a priority queue to store vertices that
    // are being preinMST. This is weird syntax in C++.
    // Refer below link for details of this syntax
    // http://geeksquiz.com/implement-min-heap-using-stl/
    priority_queue<iPair, vector<iPair>, greater<iPair>> pq;

    int src = 0; // Taking vertex 0 as source

    // Create a vector for keys and initialize all
    // keys as infinite (INF)
    vector<int> key(V, INF);

    // To store parent array which in turn store MST
    vector<int> parent(V, -1);

    // To keep track of vertices included in MST
    vector<bool> inMST(V, false);

    // Insert source itself in priority queue and initialize
    // its key as 0.
    pq.push(make_pair(0, src));
    key[src] = 0;

    /* Looping till priority queue becomes empty */
    while (!pq.empty()) {
        // The first vertex in pair is the minimum key
        // vertex, extract it from priority queue.
        // vertex label is stored in second of pair (it
        // has to be done this way to keep the vertices
        // sorted key (key must be first item
        // in pair)
        int u = pq.top().second;
        pq.pop();

        inMST[u] = true; // Include vertex in MST

        // 'i' is used to get all adjacent vertices of a vertex
        list<pair<int, int>>::iterator i;
        for (i = adj[u].begin(); i != adj[u].end(); ++i) {
            // Get vertex label and weight of current adjacent
            // of u.
            int v      = (*i).first;
            int weight = (*i).second;

            //  If v is not in MST and weight of (u,v) is smaller
            // than current key of v
            if (inMST[v] == false && key[v] > weight + fat) {
                // Updating key of v
                char buff[100];
                int  uu = u, vv = v, ww = weight;
                if (startingpoint != 'A') {
                    if ((char)v + 65 == 'A')
                        vv = startingpoint - 65;
                    else if ((char)v + 65 == startingpoint) {
                        vv = 0;
                    }
                    if ((char)u + 65 == 'A')
                        uu = startingpoint - 65;
                    else if ((char)u + 65 == startingpoint)
                        uu = 0;
                }
                snprintf(buff, sizeof(buff), "%c%d%c", (char)vv + 65, fat + ww,
                         (char)uu + 65);
                bool exist = 0;
                for (auto it = Svec.begin(); it != Svec.end(); ++it) {
                    if (it->c_str()[0] == buff[0]) {
                        exist = 1;
                        if (it->c_str()[1] > buff[1]) {
                            Svec.erase(it);
                            it--;
                            exist = 0;
                        }
                    }
                }
                if (!exist) Svec.push_back(buff);
                key[v] = ww + fat;
                pq.push(make_pair(key[v], v));
                parent[v] = u;
            }
        }
        cout << "\n";
        sort(Svec.begin(), Svec.end(), stringCompare3);
        sort(Svec.begin(), Svec.end(), stringCompare);
        sort(Svec.begin(), Svec.end(), stringCompare2);


        int  s[100];
        char tmp = '0';
        for (auto x : Svec) {
            if (x.c_str()[0] == tmp) { x.erase(); }
            tmp = x.c_str()[0];
        }

        std::string input  = Svec.begin()->c_str();
        std::string output = std::regex_replace(
            input, std::regex("[^0-9]*([0-9]+).*"), std::string("$1"));
        int num = 0;
        if (incPriority)
            for (auto ch : output) { num = (num * 10) + (ch - '0'); }
        fat = num;

        for (auto x : Svec) cout << x << endl;
        if (!Svec.empty()) Svec.erase(Svec.begin());
    }

    // Print edges of MST using parent array
    cout << "\nTree:\n";
    for (int i = 1; i < V; ++i) {
        printf("%c - %c\n", (char)parent[i] + 65, (char)i + 65);
    }
}

// Driver program to test methods of graph class
int main() {
    // create the graph given in above fugure
    // 123456789
    // abcdefghi
    /*int V = 5; // H06 2 der ”priority” er lik ”val[k] + t->w”.
    Graph g(V);

    incPriority   = 0;
    startingpoint = 'A';

    g.addEdge('A', 'B', 3);
    g.addEdge('A', 'C', 3);
    g.addEdge('A', 'D', 1);
    g.addEdge('A', 'E', 2);

    g.addEdge('B', 'E', 2);
    g.addEdge('B', 'D', 2);

    g.addEdge('C', 'D', 3);
    g.addEdge('C', 'E', 3);

    g.addEdge('D', 'E', 1);

    g.primMST();*/

    /*    int   V = 9; // s10 2 der ”priority” er lik ”val[k] + t->w”.
        Graph g(V);

        incPriority   = 1;
        startingpoint = 'I';

        g.addEdge('A', 'B', 2);
        g.addEdge('A', 'C', 3);
        g.addEdge('A', 'H', 2);

        g.addEdge('B', 'E', 3);
        g.addEdge('B', 'F', 2);
        g.addEdge('B', 'G', 2);
        g.addEdge('B', 'I', 2);

        g.addEdge('C', 'I', 3);

        g.addEdge('D', 'G', 4);
        g.addEdge('D', 'H', 3);

        g.addEdge('E', 'G', 2);

        g.addEdge('F', 'I', 3);

        printf("%c*\n\n", startingpoint);

        g.primMST();*/

    int   V = 7; // s19 2 der ”priority” er lik ”val[k] + t->w”.
    Graph g(V);

    incPriority   = 1;
    startingpoint = 'C';

    g.addEdge('A', 'D', 1);
    g.addEdge('A', 'E', 1);

    g.addEdge('B', 'C', 1);
    g.addEdge('B', 'E', 1);
    g.addEdge('B', 'F', 2);
    g.addEdge('B', 'G', 1);

    g.addEdge('C', 'F', 2);
    g.addEdge('C', 'G', 1);

    g.addEdge('D', 'G', 2);

    g.addEdge('E', 'F', 2);

    printf("%c*\n\n", startingpoint);

    g.primMST();

    return 0;
}

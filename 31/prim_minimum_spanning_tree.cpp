// C / C++ program for Prim's MST for adjacency list representation of graph
#include <bits/stdc++.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

#include <algorithm>
#include <climits>
#include <cstring>
#include <iostream>
#include <regex>
#include <string>
using namespace std;

bool duplicate(string a, string b) {
    if (a[0] == b[0]) b.clear();
    return 0;
}
bool comparator(string a, string b) { return (a[0] > b[0]); }
bool comparator2(string a, string b) { return (a[1] >= b[1]); }
bool comparator3(string a, string b) { return (a[2] < b[2]); }
// bool comparator(string a, string b) { return (a[1] >= b[1] && a[0] <= b[0]); }
#include <iostream>
bool   incPriority;
char   startingpoint = 'A';
int    stackcount    = 0;
string stackpq[100];
int    stackcountvisit = 0;
string stackpqvisit[100];

// A structure to represent a node in adjacency list
struct AdjListNode {
    int                 dest;
    int                 weight;
    struct AdjListNode* next;
};

// A structure to represent an adjacency list
struct AdjList {
    struct AdjListNode* head; // pointer to head node of list
};

// A structure to represent a graph. A graph is an array of adjacency lists.
// Size of array will be V (number of vertices in graph)
struct Graph {
    int             V;
    struct AdjList* array;
};

// A utility function to create a new adjacency list node
struct AdjListNode* newAdjListNode(int dest, int weight) {
    struct AdjListNode* newNode = (struct AdjListNode*)malloc(sizeof(struct AdjListNode));
    newNode->dest               = dest;
    newNode->weight             = weight;
    newNode->next               = NULL;
    return newNode;
}

// A utility function that creates a graph of V vertices
struct Graph* createGraph(int V) {
    struct Graph* graph = (struct Graph*)malloc(sizeof(struct Graph));
    graph->V            = V;

    // Create an array of adjacency lists. Size of array will be V
    graph->array = (struct AdjList*)malloc(V * sizeof(struct AdjList));

    // Initialize each adjacency list as empty by making head as NULL
    for (int i = 0; i < V; ++i) graph->array[i].head = NULL;

    return graph;
}

// Adds an edge to an undirected graph
void addEdge(struct Graph* graph, int src, int dest, int weight) {
    // Add an edge from src to dest. A new node is added to the adjacency
    // list of src. The node is added at the beginning
    if (startingpoint != 'A') {
        if (src == (int)startingpoint)
            src = 'A';
        else if (src == 'A')
            src = (int)startingpoint;

        if (dest == (int)startingpoint)
            dest = 'A';
        else if (dest == 'A')
            dest = (int)startingpoint;
    }

    struct AdjListNode* newNode = newAdjListNode(dest - 65, weight);
    newNode->next               = graph->array[src - 65].head;
    graph->array[src - 65].head = newNode;

    // Since graph is undirected, add an edge from dest to src also
    newNode                      = newAdjListNode(src - 65, weight);
    newNode->next                = graph->array[dest - 65].head;
    graph->array[dest - 65].head = newNode;
}

// Structure to represent a min heap node
struct MinHeapNode {
    int v;
    int key;
};

// Structure to represent a min heap
struct MinHeap {
    int                  size;     // Number of heap nodes present currently
    int                  capacity; // Capacity of min heap
    int*                 pos;      // This is needed for decreaseKey()
    struct MinHeapNode** array;
};

// A utility function to create a new Min Heap Node
struct MinHeapNode* newMinHeapNode(int v, int key) {
    struct MinHeapNode* minHeapNode =
        (struct MinHeapNode*)malloc(sizeof(struct MinHeapNode));
    minHeapNode->v   = v;
    minHeapNode->key = key;
    return minHeapNode;
}

// A utilit function to create a Min Heap
struct MinHeap* createMinHeap(int capacity) {
    struct MinHeap* minHeap = (struct MinHeap*)malloc(sizeof(struct MinHeap));
    minHeap->pos            = (int*)malloc(capacity * sizeof(int));
    minHeap->size           = 0;
    minHeap->capacity       = capacity;
    minHeap->array =
        (struct MinHeapNode**)malloc(capacity * sizeof(struct MinHeapNode*));
    return minHeap;
}

// A utility function to swap two nodes of min heap. Needed for min heapify
void swapMinHeapNode(struct MinHeapNode** a, struct MinHeapNode** b) {
    struct MinHeapNode* t = *a;
    *a                    = *b;
    *b                    = t;
}

// A standard function to heapify at given idx
// This function also updates position of nodes when they are swapped.
// Position is needed for decreaseKey()
void minHeapify(struct MinHeap* minHeap, int idx) {
    int smallest, left, right;
    smallest = idx;
    left     = 2 * idx + 1;
    right    = 2 * idx + 2;

    if (left < minHeap->size &&
        minHeap->array[left]->key < minHeap->array[smallest]->key)
        smallest = left;

    if (right < minHeap->size &&
        minHeap->array[right]->key < minHeap->array[smallest]->key)
        smallest = right;

    if (smallest != idx) {
        // The nodes to be swapped in min heap
        MinHeapNode* smallestNode = minHeap->array[smallest];
        MinHeapNode* idxNode      = minHeap->array[idx];

        // Swap positions
        minHeap->pos[smallestNode->v] = idx;
        minHeap->pos[idxNode->v]      = smallest;

        // Swap nodes
        //   swapMinHeapNode(&minHeap->array[smallest], &minHeap->array[idx]);
        swapMinHeapNode(&minHeap->array[smallest], &minHeap->array[idx]);

        minHeapify(minHeap, smallest);
    }
}

// A utility function to check if the given minHeap is ampty or not
int isEmpty(struct MinHeap* minHeap) { return minHeap->size == 0; }

// Standard function to extract minimum node from heap
struct MinHeapNode* extractMin(struct MinHeap* minHeap) {
    if (isEmpty(minHeap)) return NULL;

    // Store the root node
    struct MinHeapNode* root = minHeap->array[0];

    // Replace root node with last node
    struct MinHeapNode* lastNode = minHeap->array[minHeap->size - 1];
    minHeap->array[0]            = lastNode;

    // Update position of last node
    minHeap->pos[root->v]     = minHeap->size - 1;
    minHeap->pos[lastNode->v] = 0;

    // Reduce heap size and heapify root
    --minHeap->size;
    minHeapify(minHeap, 0);

    return root;
}

// Function to decrease key value of a given vertex v. This function
// uses pos[] of min heap to get the current index of node in min heap
void decreaseKey(struct MinHeap* minHeap, int v, int key) {
    // Get the index of v in heap array
    int i = minHeap->pos[v];

    // Get the node and update its key value
    minHeap->array[i]->key = key;

    // Travel up while the complete tree is not hepified.
    // This is a O(Logn) loop
    while (i && minHeap->array[i]->key < minHeap->array[(i - 1) / 2]->key) {
        // Swap this node with its parent
        minHeap->pos[minHeap->array[i]->v]           = (i - 1) / 2;
        minHeap->pos[minHeap->array[(i - 1) / 2]->v] = i;
        swapMinHeapNode(&minHeap->array[i], &minHeap->array[(i - 1) / 2]);

        // move to parent index
        i = (i - 1) / 2;
    }
}

// A utility function to check if a given vertex
// 'v' is in min heap or not
bool isInMinHeap(struct MinHeap* minHeap, int v) {
    if (minHeap->pos[v] < minHeap->size) return true;
    return false;
}

// A utility function used to print the constructed MST
void printArr(int arr[], int n) {
    printf("Tree:\n");
    for (int i = 1; i < n; ++i) {
        int ti = i;
        if ((char)arr[i] + 65 == 'A')
            arr[i] = startingpoint - 65;
        else if ((char)arr[i] + 65 == startingpoint)
            arr[i] = 'A' - 65;
        if ((char)i + 65 == 'A')
            ti = startingpoint - 65;
        else if ((char)i + 65 == startingpoint)
            ti = 'A' - 65;
        printf("%c - %c\n", (char)ti + 65, (char)arr[i] + 65);
    }
}

// The main function that constructs Minimum Spanning Tree (MST)
// using Prim's algorithm
void PrimMST(struct Graph* graph) {
    int V = graph->V; // Get the number of vertices in graph
    int parent[V];    // Array to store constructed MST
    int key[V];       // Key values used to pick minimum weight edge in cut

    // minHeap represents set E
    struct MinHeap* minHeap = createMinHeap(V);

    // Initialize min heap with all vertices. Key value of
    // all vertices (except 0th vertex) is initially infinite
    for (int v = 1; v < V; ++v) {
        parent[v]         = -1;
        key[v]            = INT_MAX;
        minHeap->array[v] = newMinHeapNode(v, key[v]);
        minHeap->pos[v]   = v;
    }

    // Make key value of 0th vertex as 0 so that it
    // is extracted first
    /*    key[(int)startingpoint - 65] = 0;
        minHeap->array[0] = newMinHeapNode((int)startingpoint - 65, key[0]);
        minHeap->pos[(int)startingpoint - 65] = 0;*/

    key[0]            = 0;
    minHeap->array[0] = newMinHeapNode(0, key[0]);
    minHeap->pos[0]   = 0;

    // Initially size of min heap is equal to V
    minHeap->size = V;

    // In the following loop, min heap contains all nodes
    // not yet added to MST.
    int    times = 0;
    string testsq[100];
    int    fat = 0;
    while (!isEmpty(minHeap)) {
        // Extract the vertex with minimum key value
        struct MinHeapNode* minHeapNode = extractMin(minHeap);

        int u = minHeapNode->v; // Store the extracted vertex number

        // Traverse through all adjacent vertices of u (the extracted
        // vertex) and update their key values
        struct AdjListNode* pCrawl = graph->array[u].head;

        while (pCrawl != NULL) {
            int v = pCrawl->dest;

            // If v is not yet included in MST and weight of u-v is
            // less than key value of v, then update key value and
            // parent of v
            if (isInMinHeap(minHeap, v) && pCrawl->weight + fat < key[v] &&
                pCrawl->dest) {
                bool st = (startingpoint != 'A');
                // printf("%c%d%c\n", (char)v + 65, pCrawl->weight, tolower((char)u + 65));
                if ((char)v + 65 == 'A' && st)
                    stackpq[stackcount] += startingpoint;
                else if ((char)v + 65 == startingpoint && st)
                    stackpq[stackcount] += 'A';
                else
                    stackpq[stackcount] += (char)v + 65;

                stackpq[stackcount] += to_string(pCrawl->weight + fat);

                if ((char)u + 65 == 'A' && st)
                    stackpq[stackcount] += tolower(startingpoint);
                else if ((char)u + 65 == startingpoint && st)
                    stackpq[stackcount] += 'a';
                else
                    stackpq[stackcount] += tolower((char)u + 65);
                stackcount++;
                key[v]    = pCrawl->weight + fat;
                parent[v] = u;
                decreaseKey(minHeap, v, key[v]);
            }
            pCrawl = pCrawl->next;
        }
        /*//sort(stackpq,stackpq,comparator);
        sort(stackpq,stackpq+stackcount,comparator3);
        sort(stackpq,stackpq+stackcount,comparator);
        sort(stackpq,stackpq+stackcount,comparator2);
        for (int i = 0; i < stackcount; i++)
            printf("%s", stackpq[i].c_str());
        //TODO make a print function here that sorts the strings
        //stackpq[0] = stackpq[stackcount--];
        stackcount--;
        std::cout << "\n";*/

        // sort(stackpq,stackpq,comparator);
        /*sort(stackpq, stackpq + stackcount, comparator3);
        sort(stackpq, stackpq + stackcount, comparator);
        sort(stackpq, stackpq + stackcount, comparator2);*/

        // sort(stackpq, stackpq + stackcount-1, comparator);
        // sort(stackpq, stackpq + stackcount-1, comparator);
        sort(stackpq, stackpq + stackcount, comparator);
        sort(stackpq, stackpq + stackcount, comparator3);
        sort(stackpq, stackpq + stackcount, comparator2);
        bool same = 1;
        if (times != stackcount) same = 0;
        if (true)
            for (int i = stackcount; i >= 0; i--) {
                printf("%s\n", stackpq[i].c_str());
                testsq[i] = stackpq[i];
            }
        // TODO make a print function here that sorts the strings
        // stackpq[0] = stackpq[stackcount--];
        times              = stackcount - 1;
        std::string input  = stackpq[stackcount - 1];
        std::string output = std::regex_replace(
            input, std::regex("[^0-9]*([0-9]+).*"), std::string("$1"));
        int num = 0;
        if (incPriority)
            for (auto ch : output) { num = (num * 10) + (ch - '0'); }
        fat = num;
        std::cout << "\n";
        if (!stackpq[stackcount - 1].empty()) {
            std::cout << "\n Clearing:" << stackpq[stackcount - 1] << "\n";
            stackpq[--stackcount].clear();
        }
    }

    // print edges of MST
    printArr(parent, V);
}

// Driver program to test above functions
int main() {
    // V =123456789
    //    ABCDEFGHI
    // Let us create the graph given in above fugure
    /*
        int           V     = 7; // h18 2c der "priority" er lik "t->w",
        incPriority = 0;
        startingpoint = 'A';
        struct Graph* graph = createGraph(V);

        addEdge(graph, 'A', 'D', 1);
        addEdge(graph, 'A', 'E', 1);
        addEdge(graph, 'A', 'F', 1);

        addEdge(graph, 'B', 'D', 2);
        addEdge(graph, 'B', 'F', 2);
        addEdge(graph, 'B', 'G', 2);

        addEdge(graph, 'C', 'D', 2);
        addEdge(graph, 'C', 'F', 2);

        addEdge(graph, 'E', 'F', 2);
        addEdge(graph, 'E', 'G', 2);
        printf("%c*\n\n",startingpoint);
        PrimMST(graph);
        */
    /*
    int V = 9; // s10 2 der ”priority” er lik ”val[k] + t->w”.

    struct Graph* graph = createGraph(V);
    incPriority         = 1;
    startingpoint       = 'I';

    addEdge(graph, 'A', 'B', 2);
    addEdge(graph, 'A', 'C', 3);
    addEdge(graph, 'A', 'H', 2);

    addEdge(graph, 'B', 'E', 3);
    addEdge(graph, 'B', 'F', 2);
    addEdge(graph, 'B', 'G', 2);
    addEdge(graph, 'B', 'I', 2);

    addEdge(graph, 'C', 'I', 3);

    addEdge(graph, 'D', 'G', 4);
    addEdge(graph, 'D', 'H', 3);

    addEdge(graph, 'E', 'G', 2);

    addEdge(graph, 'F', 'I', 3);

    printf("%c*\n\n", startingpoint);
    PrimMST(graph);
    */
    /*
        int V = 7; // s19 2 der ”priority” er lik ”val[k] + t->w”.

        struct Graph* graph = createGraph(V);
        incPriority         = 1;
        startingpoint       = 'C';

        addEdge(graph, 'A', 'D', 1);
        addEdge(graph, 'A', 'E', 1);

        addEdge(graph, 'B', 'C', 1);
        addEdge(graph, 'B', 'E', 1);
        addEdge(graph, 'B', 'F', 2);
        addEdge(graph, 'B', 'G', 1);

        addEdge(graph, 'C', 'F', 2);
        addEdge(graph, 'C', 'G', 1);

        addEdge(graph, 'D', 'G', 2);

        addEdge(graph, 'E', 'F', 2);

        printf("%c*\n\n", startingpoint);
        PrimMST(graph);
        */
    int V = 5; // s10 2 der ”priority” er lik ”val[k] + t->w”.

    struct Graph* graph = createGraph(V);
    incPriority         = 0;
    startingpoint       = 'A';

    addEdge(graph, 'A', 'B', 3);
    addEdge(graph, 'A', 'C', 3);
    addEdge(graph, 'A', 'D', 1);
    addEdge(graph, 'A', 'E', 2);

    addEdge(graph, 'B', 'E', 2);
    addEdge(graph, 'B', 'D', 2);

    addEdge(graph, 'C', 'D', 3);
    addEdge(graph, 'C', 'E', 3);

    addEdge(graph, 'D', 'E', 1);

    printf("%c*\n\n", startingpoint);
    PrimMST(graph);
    return 0;
}


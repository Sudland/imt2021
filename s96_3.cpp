#include <iostream>
using namespace std;

typedef char itemType;

char key[16] = " TANNREGULERING";

void shellsort(itemType a[], int N) { // Oppgave A
    int      i, j, h;
    itemType v;
    for (h = 1; h <= N / 9; h = 3 * h + 1)
        ;
    for (; h > 0; h /= 3)
        for (i = h + 1; i <= N; i += 1) {
            v = a[i]; // Merk at a[0] er ' '
            j = i;
            while (j > h && a[j - h] > v) {
                a[j] = a[j - h];
                j -= h;
            }
            a[j] = v;
            cout << "\nArrayen: " << a << " h:" << h << " i:" << i;
        }
}

int main() {
    shellsort(key, 14); // A

    cout << "\n\n" << key; //
}

/* C implementation QuickSort */
#include <iostream>
#include <stack>
using namespace std;
bool infected[100];
/* Function to print an array */
void printArray(int arr[], int size) {
    int i;
    for (i = 0; i < size; i++) printf("%c ", arr[i]);
}
void printArraylen(int arr[], int low, int high) {
    int i;
    for (i = 0; i < low; i++) { cout << "  "; }
    for (i = 0; i < high; i++) {
        if (low <= i && high >= i)
            if (infected[i])
                printf("%c ", arr[i]);
            else
                printf("%c ", tolower(arr[i]));
    }
}

int partition(int array[], int l, int r) {
    // Dreie-element skal ligge i r skuffen så
    //   vi begynner der, og en venstre fra l:
    int lp = l - 1;
    int rp = r;
    // Partisjoner slik at venstre halvdel er mindre og høyre er større:
    while (true) {
        while (array[++lp] < array[r])
            ;
        while (array[--rp] > array[r])
            ;
        if (lp >= rp) break;
        swap(array[lp], array[rp]);
    } // Legg dreie-elementet i sin nå LÅSTE sorterte posisjon:
    swap(array[lp], array[r]);
    infected[lp] = 1;
    printArraylen(array, l, r+1);
    cout << '\n';
    return lp; // Returner indexen
}
void quickSort(int a[], int l, int r) // s.116
{
    int i;
    if (r > l) {
        i = partition(a, l, r);
        quickSort(a, l, i - 1);
        quickSort(a, i + 1, r);
        // printArraylen(a, start, end);
    }
}

int main() {
    //    int arr[] = {16, 18, 15, 19, 5, 4, 25, 18, 5};
    // int arr[] = {8, 9, 7, 8, 7, 1, 20, 5};
    // int arr[] = {'N', 'E', 'W', 'C', 'A', 'S', 'T', 'L', 'E'}; // h16 1b
    //int arr[] = {'E', 'M', 'I', 'R', 'A', 'T', 'E', 'S'}; // h06 1b
     //int arr[] = {'H', 'I', 'G', 'H', 'B', 'U', 'R', 'Y'}; // h05 1b
    // int arr[] = {'R', 'A', 'N', 'G', 'E', 'R', 'S'}; // s10 1a
     int arr[] = {'A', 'L', 'L', 'E', 'R', 'S','I','S','T','E'}; // s10 1a
    // int arr[] = {'P', 'I', 'E', 'M', 'O', 'N', 'T', 'E'}; // s18 1c
    // int arr[] = {'P', 'R', 'O', 'S', 'E', 'D', 'Y', 'R','E'};
    // int arr[] = {'H', 'I', 'G', 'H', 'G', 'A', 'T', 'E'};
    int n = sizeof(arr) / sizeof(arr[0]);
    for (int i = 0; i < 100; i++) infected[i] = 0;
    quickSort(arr, 0, n - 1);
    printf("Sorted array: ");
    printArray(arr, n);
    return 0;
}



// C++ implementation of Shell Sort
#include <iostream>

void printArray(int arr[], int n) {
    for (int i = 0; i < n; i++) std::cout << (char)arr[i] << "";
}
/* function to sort arr using shellSort */
void shellSort(int arr[], int n) {
    int touched[100];
    for (int i = 0; i < 100; i++) touched[i] = 0;
    // Start with a big gap, then reduce the gap
    for (int gap = n / 2; gap > 0; gap /= 2) {
        // Do a gapped insertion sort for this gap size.
        // The first gap elements arr[0..gap-1] are already in gapped order
        // keep adding one more element until the entire array is
        // gap sorted
        if (gap == 1 || gap == 4) {
            std::cout << std::endl;
            for (int i = gap; i < n; i += 1) {
                // add arr[i] to the elements that have been gap sorted
                // save arr[i] in temp and make a hole at position i
                int temp = arr[i];

                // shift earlier gap-sorted elements up until the correct
                // location for arr[i] is found
                int  j;
                int  times    = 0;
                char str[100] = "";
                // std::cout << "\n";
                for (j = i; j >= gap && arr[j - gap] > temp; j -= gap) {
                    touched[j]   = 1;
                    arr[j]       = arr[j - gap];
                    str[times++] = (char)arr[j];
                }
                // std::cout << "\n";

                // put temp (the original arr[i]) in its correct location
                int del    = arr[j];
                touched[j] = 1;
                arr[j]     = temp;
                std::cout << "H:" << gap << " "
                          << "I:" << i + 1 << "\t";
                // printArray(arr, n);
                for (int x = 0; x < n; x++) {
                    if (touched[x] && j != i)
                        std::cout << (char)arr[x];
                    else
                        std::cout << (char)tolower(arr[x]);
                }

                if (j != i) {
                    std::cout << "\t "
                              << "'" << (char)temp << "'"
                              << " vandrer forbi ";
                    for (int x = 0; x < 100; x++) {
                        bool firsttime = 0;
                        if (touched[x] && x != j)
                            std::cout << (char)arr[x] << " ";
                    }
                } else
                    std::cout << "\t\t(Ingenting skjer)";
                std::cout << "\n";
                for (int i = 0; i < 100; i++) touched[i] = 0;
            }
        }
    }
}

int main() {
    // int arr[] = {'S', 'I', 'L', 'L', 'E', 'D', 'I', 'L', 'L'};
    // int arr[] = {'D', 'R', 'A', 'K', 'T', 'E', 'N', 'E', 'S'};
    int arr[] = {'S', 'U', 'M', 'P', 'S', 'V', 'A', 'M', 'P'}; // h04 1a)

    int n = sizeof(arr) / sizeof(arr[0]);

    // std::cout << "Array before sorting: \n";
    // printArray(arr, n);

    shellSort(arr, n);

    //    std::cout << "Array after sorting: \n";
    //  printArray(arr, n);
}

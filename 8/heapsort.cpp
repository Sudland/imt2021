
// C++ program for implementation of Heap Sort
#include <iostream>

using namespace std;
int  arrsize;
int  infected[100];
char tmparr[100] = "                                      ";
void swap(char& a, char& b) {
    int temp = a;
    a        = b;
    b        = temp;
}
void printArraysort(char arr[], int n) {
    for (int i = 0; i < n; ++i)
        if (infected[i])
            cout << arr[i] << "";
        else
            cout << (char)tolower(arr[i]) << "";
    cout << "\n";
    for (int i = 0; i < n; ++i)
        if (infected[i]) cout << i << " ";
    cout << "\n";
}

void printspaces(int i) {
    for (int k = 0; i > k; k++) cout << " ";
}

/* A utility function to print array of size n */
void printArray(char arr[], int n) {
    for (int i = 0; i < n; ++i)
        // if (infected[i])
        cout << arr[i] << "";
    // else
    //       cout << (char)tolower(arr[i]) << "";
    cout << "\n";
}

// To heapify a subtree rooted with node i which is
// an index in arr[]. n is size of heap
void heapify(char arr[], int n, int i, int set = 0) {
    int largest = i;         // Initialize largest as root
    int l       = 2 * i + 1; // left = 2*i + 1
    int r       = 2 * i + 2; // right = 2*i + 2
    // If left child is larger than root
    if (l < n && arr[l] > arr[largest]) largest = l;

    // If right child is larger than largest so far
    if (r < n && arr[r] > arr[largest]) largest = r;

    // If largest is not root
    //
    if (largest != i) {
        infected[largest] = 1;
        infected[i]       = 1;
        swap(arr[i], arr[largest]);

        // Recursively heapify the affected sub-tree
        heapify(arr, n, largest, 1);

        if (arr[i] != 0 && arr[l] != 0 && arr[r] != 0) {
            tmparr[i] = arr[i];
            tmparr[l] = arr[l];
            tmparr[r] = arr[r];
        }
    } else if (arr[i] != 0 && arr[l] != 0 && arr[r] != 0) {
        tmparr[i] = arr[i];
        tmparr[l] = arr[l];
        tmparr[r] = arr[r];
    }
}

// main function to do heap sort
void heapSort(char arr[], int n) {
    // Build heap (rearrange array)

    for (int i = n / 2 - 1; i >= 0; i--) {
        for (int i = 0; i < n; ++i) infected[i] = 0;

        heapify(arr, n, i, 0);
        for (int i = 0; i < n; ++i)
            if (infected[i])
                cout << tmparr[i];
            else
                cout << (char)tolower(tmparr[i]);
        cout << "\n";
        for (int i = 0; i < 100; ++i) tmparr[i] = ' ';
    }

    cout << "Downheap:\n";
    printArray(arr, n);
    cout << "\n";

    // One by one extract an element from heap

    for (int i = n - 1; i > 0; i--) {
        for (int i = 0; i < n; ++i) infected[i] = 0;
        // Move current root to end
        infected[i] = 1;
        infected[0] = 1;
        swap(arr[0], arr[i]);

        // call max heapify on the reduced heap
        heapify(arr, i, 0);
        printArraysort(arr, n);
    }
}

// Driver program
int main() {
    //char arr[] = "KNATTHOLMEN"; // h17 1c bottom og heapsort)
    char arr[] = "DIRESTRAITS"; // s10 1b
    // char arr[100] = "SLIMBILLEN";
    int n   = sizeof(arr) / sizeof(arr[0]) - 1;
    arrsize = n;

    cout << arr << '\n';
    heapSort(arr, n);

    cout << "Sorted array is \n";
    printArray(arr, n);
}


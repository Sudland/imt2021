// Counting sort in C++ programming

#include <iostream>
using namespace std;
int M = 0;

void countSort(int array[], int size) {
    // The size of count must be at least the (max+1) but
    // we cannot assign declare it as int count(max+1) in C++ as
    // it does not support dynamic memory allocation.
    // So, its size is provided statically.
    int output[100];
    int count[100];
    bool touched[100];
    int max = array[0];

    // Find the largest element of the array
    for (int i = 1; i < size; i++) {
        if (array[i] > max) max = array[i];
    }

    // Initialize count array with all zeros.
    for (int i = 0; i <= max; ++i) { count[i] = 0; }

    // Store the count of each element
    for (int i = 0; i < size; i++) { count[array[i]]++; }

    cout << endl << "Etter 2.for-løkke - count:  ";
    for (int i = 0; i < M; i++) { cout << count[i] << " "; }

    // Store the cummulative count of each array
    for (int i = 1; i <= max; i++) { count[i] += count[i - 1]; }
    cout << endl << "Etter 3.for-løkke - count:  ";
    for (int i = 0; i < M; i++) { cout << count[i] << " "; }

    // Find the index of each element of the original array in count array, and
    // place the elements in output array
    cout << endl << "MIDT i 4.for-løkke - count:  ";
    for (int i = (size - 1); i >= 0; i--) {
        output[count[array[i]] - 1] = array[i];
        touched[count[array[i]] - 1] = 1;
        count[array[i]]--;
        if (i == (size / 2) ) {
            for (int j = 0; j < M; j++) { cout << count[j] << " "; }
            cout << endl << "               -   b:  ";
            for (int j = 0; j < size; j++) {
                if(!output[j] && !touched[j])
                    cout << "- ";
                else
                    printf("%c ",(char)output[j]+65);
            }
        }
    }
    cout << endl;
    // Copy the sorted elements into original array
}

// Function to print an array
void printArray(int array[], int size) {
    for (int i = 0; i < size; i++) cout << array[i] << " ";
    cout << endl;
}

// Driver code
int main() {
    //char arr[] = "BACBACCABABBAC"; //s19 1b
    //M           = 3; // s19 1c
    char arr[] = "ABCABCAABCAABCCC"; //h14 1a
    M           = 3; // h14 1a

    int arsize = sizeof(arr) / sizeof(arr[0]);
    int array[arsize];
    for(int i = 0 ;i <arsize;i++)
        array[i] = (int)arr[i]-65;

    int n       = sizeof(array) / sizeof(array[0]);
    countSort(array, n-1);
}

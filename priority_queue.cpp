
// Note that by default C++ creates a max-heap
// for priority queue
#include <iostream>
#include <queue>

using namespace std;

void showpq(priority_queue <int> gq)
{
    priority_queue <int> g = gq;
    while (!g.empty())
    {
        cout << '\t' << g.top();
        g.pop();
    }
    cout << '\n';
}

int main ()
{
    priority_queue <int> gquiz;
    gquiz.push(98);
    gquiz.push(87);
    gquiz.push(72);
    gquiz.push(49);
    gquiz.push(73);
    gquiz.push(70);
    gquiz.push(70);
    gquiz.push(45);
    gquiz.push(40);
    gquiz.push(46);
    gquiz.push(42);
    gquiz.push(31);
    gquiz.push(39);

    cout << "The priority queue gquiz is : ";
    showpq(gquiz);

    cout << "\ngquiz.size() : " << gquiz.size();
    cout << "\ngquiz.top() : " << gquiz.top();


    cout << "\n";

    showpq(gquiz);
    gquiz.push(87);
    gquiz.push(86);
    showpq(gquiz);

    return 0;
}


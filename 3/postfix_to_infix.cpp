#include <bits/stdc++.h>

#include <string>

#include "exprtk.hpp"
using namespace std;

bool isOperand(char x) {
    return (x >= 'a' && x <= 'z') || (x >= 'A' && x <= 'Z') || (x >= '0' && x <= '9');
}

// Get Infix for a given postfix
// expression
void PrintStack(stack<string> s, string& str) {
    typedef exprtk::expression<float> expression_t;
    typedef exprtk::parser<float>     parser_t;
    expression_t                      expression;
    parser_t                          parser;
    // If stack is empty then return
    if (s.empty()) return;

    string x = s.top();

    // Pop the top element of the stack
    s.pop();

    // Recursively call the function PrintStack
    PrintStack(s, str);

    // Print the stack element starting
    // from the bottom
    if (parser.compile(x, expression)) {
        int val = expression.value();
        str.append(to_string(val));
    }
    //    str.append(x);
    str.append(" ");

    // Push the same element onto the stack
    // to preserve the order
    s.push(x);
}
string getInfix(string exp) {
    stack<string> s;

    for (int i = 0; exp[i] != '\0'; i++) {
        string tmpstr;
        // Push operands
        if (isOperand(exp[i])) {
            string op(1, exp[i]);
            s.push(op);
            // cout << op;
        }

        // We assume that input is
        // a valid postfix and expect
        // an operator.
        else {
            string op1 = s.top();
            s.pop();
            string op2 = s.top();
            s.pop();
            s.push("(" + op2 + exp[i] + op1 + ")");
        }
        string tmpstr2;
        PrintStack(s, tmpstr2);
        cout << tmpstr2 << '|';
    }

    // There must be a single element
    // in stack now which is the required
    // infix.
    return s.top();
}

// Driver code
int main() {
    typedef exprtk::expression<double> expression_t;
    typedef exprtk::parser<double>     parser_t;

    string exp = "34+32*+2+53*42+*+"; // s16 1a
    /*
(  skrevet Infix:     ((((3 + 4) + (3 * 2)) + 2) + ((5 * 3) * (4 + 2)))   )


    og har svaret:        105
                                                            2
                                  2                3     4  4  6
                            4   3 3 6     2     5  5 15 15 15 15 90
    Stakken underveis:  - 3 3 7 7 7 7 13 13 15 15 15 15 15 15 15 15 105 -
*/

    cout << "Stacken underveis"
         << " _|";
    string Infix = getInfix(exp);
    cout << " _|";
    cout << endl << endl;
    expression_t expression;
    parser_t     parser;
    double       result = 0;
    if (parser.compile(Infix, expression)) { result = expression.value(); }
    cout << endl << "skrevet Infix: " << Infix; // ((7*3)+(((4*5)*(3+2))*(6+3)))
    cout << endl << endl << "Og har svaret:" << result << endl << endl;

    return 0;
}

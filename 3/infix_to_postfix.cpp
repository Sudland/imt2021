#include <stdio.h>

#include <iostream>
#include <stack>
#include <string>
using namespace std;
void PrintStack(stack<char> s, string& str) {
    // If stack is empty then return
    if (s.empty()) return;

    int x = s.top();

    // Pop the top element of the stack
    s.pop();

    // Recursively call the function PrintStack
    PrintStack(s, str);

    // Print the stack element starting
    // from the bottom
    if ((char)x != '(' && (char)x != ' ') str += (char)x;

    // Push the same element onto the stack
    // to preserve the order
    s.push(x);
}

// Simply determine if character is one of the four standard operators.
bool isOperator(char character) {
    if (character == '+' || character == '-' || character == '*' || character == '/') {
        return true;
    }
    return false;
}

// If the character is not an operator or a parenthesis, then it is assumed to
// be an operand.
bool isOperand(char character) {
    if (!isOperator(character) && character != '(' && character != ')') {
        return true;
    }
    return false;
}

// Compare operator precedence of main operators.
// Return 0 if equal, -1 if op2 is less than op1, and 1 if op2 is greater than
// op1.
int compareOperators(char op1, char op2) {
    if ((op1 == '*' || op1 == '/') && (op2 == '+' || op2 == '-')) {
        return -1;
    } else if ((op1 == '+' || op1 == '-') && (op2 == '*' || op2 == '/')) {
        return 1;
    }
    return 0;
}

int main() {
    // Empty character stack and blank postfix string.
    stack<char> opStack;
    string      postFixString = "";

    // char input[100] = "(((6+4)*(3+2))*(((3+3)+(1+3))*5))"; //?
//    char input[100] = "((((3 + 2) * (4 * 3)) * (5 + 3)) * (2 + 3))"; // h13 1a
    /*
    Skrevet POSTFIX blir:  3 2 + 4 3 * * 5 3 + * 2 3 + *

                                   *       +       +
    Stakken underveis:     _ + _ * * * _ * * * _ * * * _
    */

    char input[100] = "(((((4*6)+(5*3))*(5+6))+8)*3)"; // s19 1a
//char input[100] = "((7 * 3) + (((4 * 5) * (3 + 2)) * (6 + 3)))"; // s19 1a
    /*
    Skrevet POSTFIX blir:  7 3 * 4 5 * 3 2 + * 6 3 + * +

                                         +       +
                                   *   * * *   * * *
    Stakken underveis:     _ * _ + + + + + + + + + + + _
    */

    // Collect input

    // Get a pointer to our character array.
    char* cPtr = input;

    // Loop through the array (one character at a time) until we reach the end
    // of the string.
    cout << "_|"; //Stack always starts empty
    while (*cPtr != '\0') {
        string tmpstr;
        PrintStack(opStack, tmpstr);
        // If operand, simply add it to our postfix string.
        // If it is an operator, pop operators off our stack until it is empty,
        // an open parenthesis or an operator with less than or equal
        // precedence.
        if (isOperand(*cPtr)) {
            postFixString += *cPtr;
        } else if (isOperator(*cPtr)) {
            while (!opStack.empty() && opStack.top() != '(' &&
                   compareOperators(opStack.top(), *cPtr) <= 0) {
                postFixString += opStack.top();
                opStack.pop();
            }
            opStack.push(*cPtr);
        }
        // Simply push all open parenthesis onto our stack
        // When we reach a closing one, start popping off operators until we run
        // into the opening parenthesis.
        else if (*cPtr == '(') {
            opStack.push(*cPtr);
        } else if (*cPtr == ')') {
            while (!opStack.empty()) {
                if (opStack.top() == '(') {
                    opStack.pop();
                    break;
                }
                postFixString += opStack.top();
                opStack.pop();
            }
        }
        string tmpstr2;
        PrintStack(opStack, tmpstr2);
        if (tmpstr2 != tmpstr) {
            if (tmpstr2 == "")
                cout << "_";
            else
                cout << tmpstr2;
            cout << "|";
        }
        // Advance our pointer to next character in string.
        cPtr++;
    }

    // After the input expression has been ran through, if there is any
    // remaining operators left on the stack pop them off and put them onto the
    // postfix string.
    while (!opStack.empty()) {
        postFixString += opStack.top();
        opStack.pop();
    }

    // Show the postfix string at the end.
    cout << endl << "Postfix is: " << postFixString << endl; // 73*45*32+*63+*+
    return 0;
}


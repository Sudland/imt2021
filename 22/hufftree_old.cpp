#include <stdio.h>
#include <string.h>

#include <algorithm>
#include <climits> // for CHAR_BIT
#include <iomanip>
#include <iostream>
#include <iterator>
#include <map>
#include <queue>

using namespace std;
int    k[100 + 1];
int    arrcount[100 + 1];
int    code[100 + 1];
int    len[100 + 1];
int    dad[100 + 1];
string alpha;

const int   UniqueSymbols = 1 << CHAR_BIT;
const char* SampleString  = "this is an example for huffman encoding";

typedef std::vector<bool>        HuffCode;
typedef std::map<char, HuffCode> HuffCodeMap;

class INode {
public:
    const int f;
    const int c;

    virtual ~INode() {}

protected:
    INode(int f) : f(f), c(c) {}
    INode(int f, int c) : f(f), c(c) {}
};

class InternalNode : public INode {
public:
    INode* const left;
    INode* const right;

    InternalNode(INode* c0, INode* c1)
        : INode(c0->f + c1->f), left(c0), right(c1) {}
    ~InternalNode() {
        delete left;
        delete right;
    }
};

class LeafNode : public INode {
public:
    const char c;

    LeafNode(int f, char c) : INode(f, c), c(c) {}
};

struct NodeCmp {
    bool operator()(const INode* lhs, const INode* rhs) const {
        return lhs->f > rhs->f;
    }
};

INode* setupdad(const int count[]) {
    std::priority_queue<INode*, std::vector<INode*>, NodeCmp> trees;
    int                                                       i = 0;
    for (i = 0; i <= 26; ++i) {
        if (count[i] != 0) trees.push(new LeafNode(count[i], (char)i));
    }
    while (trees.size() > 1) {
        INode* childR = trees.top();
        trees.pop();

        INode* childL = trees.top();
        trees.pop();
        dad[childR->c] = i;
        dad[childL->c] = -i;
        cout << childR->c << " ";
        cout << childL->c << " ";

        INode* parent = new InternalNode(childR, childL);
        trees.push(parent);
        i++;
    }
    return trees.top();
}

void swap(int a[], int i, int j) // s.95
{
    int t = a[i];
    a[i]  = a[j];
    a[j]  = t;
}
void printArray(int a[], int n, string str) {
    cout << str << setw(15 - str.size());
    for (int i = 0; i < n; i++) {
        if (a[i] != 0 || (arrcount[i] && str == "dad[k]:"))
            cout << a[i] << "\t";
    }
}

class PQ {
private:
    int* a;
    int *p, *info;
    int  N;

public:
    PQ(int size) {
        a    = new int[size];
        p    = new int[size];
        info = new int[size];
        N    = 0;
    }
    ~PQ() {
        delete a;
        delete p;
        delete info;
    }
    void insert(int v, int x) {
        a[++N]  = v; // Store queue nr
        p[x]    = N;
        info[N] = x; // Store number/char data
    }
    void change(int x, int v) { a[p[x]] = v; }
    int  remove() // remove smallest
    {
        int j, min = 1;
        for (j = 2; j <= N; j++) {
            if (a[j] < a[min]) min = j; // TODO the array should be a heap
        }
        swap(a, min, N);
        swap(info, min, N);
        p[info[min]] = min;
        // cout << info[N] << " "; // 18 16 15 13 25 28
        return info[N--];
    }
    int empty() { return (N <= 0); }
};

PQ pq = PQ(100);

void printArray(string a, int n, string str) {
    cout << str << setw(15 - str.size());
    for (int i = 0; i < n; i++) { cout << a[i] << "\t"; }
    cout << endl << 'K' << setw(15 - 1);
    for (int i = 0; i < n; i++) {
        if (a[i] == ' ')
            cout << 0 << "\t";

        else
            cout << (int)a[i] - 64 << "\t";
    }
}
void uniqueCount(int arr[], int n, string str) {
    for (int i = 0; i <= n; i++) {
        if (str[i] == ' ')
            arr[0]++;
        else
            arr[str[i] - 64]++;
    }
}

void setupdad2(int count[]) { // TODO
    int i, t1, t2;
    for (i = 0; i <= 26; i++) // s.328
        if (count[i]) pq.insert(count[i], i);

    for (; !pq.empty(); i++) {
        t1       = pq.remove();
        t2       = pq.remove();
        dad[i]   = 0;
        dad[t1]  = i;
        dad[t2]  = -i;
        count[i] = count[t1] + count[t2];
        if (!pq.empty()) { pq.insert(count[i], i); };
    }
}

int main() {
    for (int i = 0; i < 100; i++) arrcount[i] = 0;
    // string str = "IPSWICH MOT LEEDS SAMT WEST HAM MOT SWANSEA ER MORSOMT"; //h14_2 a
    string str =
        "SILDESALATEN SYNES SPESIELT SALT OG SYRLIG SIDEN DEN SMAKTE SLIK"; // s10_2a
    string sortstr = str;
    sort(sortstr.begin(), sortstr.end());
    sortstr.erase(unique(sortstr.begin(), sortstr.end()), sortstr.end());

    uniqueCount(arrcount, str.size(), str);
    printArray(sortstr, sortstr.size(), "");
    cout << endl;
    printArray(arrcount, str.size(), "count[k]:");
    cout << endl << endl;
    printArray(sortstr, sortstr.size(), "");
    cout << endl;
    printArray(arrcount, str.size(), "count[k]:");
    setupdad2(arrcount);
    cout << endl;
    printArray(dad, 27, "dad[k]:");
    cout << endl;
    printArray(arrcount, str.size(), "count[k]:");

    /*    cout << endl;
    for (int k = 0; k <= 26; k++) //s.329
      {
        int i = 0; int x = 0; int j = 1;
        if (arrcount[k])
          for (int t=dad[k]; t; t=dad[t], j+=j, i++)
            if (t < 0) { x += j; t = -t; }
        code[k] = x; len[k] = i;
        cout << x << " ";
      }*/

    //    printArray()
}

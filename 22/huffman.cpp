#include <stdio.h>
#include <string.h>

#include <algorithm>
#include <climits> // for CHAR_BIT
#include <iomanip>
#include <iostream>
#include <iterator>
#include <map>
#include <queue>

using namespace std;
int    k[100 + 1];
int    arrcount[100 + 1];
int    code[100 + 1];
int    len[100 + 1];
int    dad[100 + 1];
string alpha;

struct compare {
    bool operator()(const int& a, const int& b) { return a > b; }
};

void swap(int a[], int i, int j) // s.95
{
    int t = a[i];
    a[i]  = a[j];
    a[j]  = t;
}
void printArray(int a[], int min, int max, string str) {
    cout << str << setw(15 - str.size());
    for (int i = min; i < max; i++) {
        if (i > min && i < max)
            if (a[i] != 0 ||
                (arrcount[i] && (str == "dad[k]:" || str == "len[k]:" || str == "code[k]:")))
                cout << a[i] << "\t";
    }
}
void printArray(int a[], int n, string str) {
    cout << str << setw(15 - str.size());
    for (int i = 0; i < n; i++) {
        if (a[i] != 0 ||
            (arrcount[i] && (str == "dad[k]:" || str == "len[k]:" || str == "code[k]:")))
            cout << a[i] << "\t";
    }
}

class PQ {
private:
    int* a;
    int *p, *info;
    int  N;

public:
    PQ(int size) {
        a    = new int[size];
        p    = new int[size];
        info = new int[size];
        N    = 0;
    }
    ~PQ() {
        delete a;
        delete p;
        delete info;
    }
    void change(int x, int v) { a[p[x]] = v; }
    int  empty() { return (N <= 0); }

    void upheap(int k) {
        int j, w;
        int v;
        v    = a[k];
        w    = info[k];
        a[0] = 0; // Sentinel.
        while (a[k / 2] >= v) {
            j       = k / 2;
            a[k]    = a[j];
            info[k] = info[j];
            k       = j;
        }
        a[k]    = v;
        info[k] = w;
    }

    void insert2(int v) {
        a[++N] = v;
        upheap(N);
    }

    /*void insert(int v, int x) {
        a[++N]  = v; // Store queue nr
        p[x]    = N;
        info[N] = x; // Store number/char data
    }*/
    void insert(int v, int x) // x = [0..nmax-1]
    {
        a[++N]  = v;
        info[N] = x;
        upheap(N);
    }

    void downheap(int k) {
        int j, w;
        int v;
        v = a[k];
        w = info[k];
        while (k <= N / 2) {
            j = k + k;
            if (j < N && a[j] > a[j + 1]) j++;
            if (v <= a[j]) break;
            a[k]    = a[j];
            info[k] = info[j];
            k       = j;
        }
        a[k]    = v;
        info[k] = w;
    }

    void downheap2(int k) // s.152
    {
        int j;
        int v;
        v = a[k];
        while (k <= N / 2) {
            j = k + k;
            if (j < N && a[j] < a[j + 1]) j++;
            if (v >= a[j]) break;
            a[k] = a[j];
            k    = j;
        }
        a[k] = v;
    }

    int remove() // Remove smallest element
    {
        int w   = info[1];
        a[1]    = a[N];
        info[1] = info[N];
        N--;
        downheap(1);
        return w;
    }
};

PQ pq = PQ(100);

void printArray(string a, int n, string str) {
    cout << str << setw(15 - str.size());
    for (int i = 0; i < n; i++) { cout << a[i] << "\t"; }
    cout << endl << 'K' << setw(15 - 1);
    for (int i = 0; i < n; i++) {
        if (a[i] == ' ')
            cout << 0 << "\t";

        else
            cout << (int)a[i] - 64 << "\t";
    }
}
void uniqueCount(int arr[], int n, string str) {
    for (int i = 0; i <= n; i++) {
        if (str[i] == ' ')
            arr[0]++;
        else
            arr[str[i] - 64]++;
    }
}

void setupdad2(int count[]) { // TODO
    priority_queue<int, vector<int>, compare> pqlow;
    int                                       i, t1, t2;
    for (i = 0; i <= 26; i++) // s.328
        if (count[i]) pq.insert(count[i], i);

    for (; !pq.empty(); i++) {
        t1      = pq.remove();
        t2      = pq.remove();
        dad[i]  = 0;
        dad[t1] = i;
        dad[t2] = i;
        // dad[t2]  = -i;
        count[i] = count[t1] + count[t2];
        if (!pq.empty()) { pq.insert(count[i], i); };
    }
    const int sizedad = 100;
    int       copydad[sizedad];
    bool      touched[sizedad];
    for (int i = 0; i <= sizedad; i++) touched[i] = 0;
    dad[-2] = 1000;
    for (int j = 0; j <= sizedad; j++) {
        int min = -2;
        for (int i = 0; i <= sizedad; i++) {
            if ((!touched[i] && dad[i] && dad[i] < dad[min])) { min = i; }
        }
        if (dad[min] != 1000) {
            copydad[j]   = dad[min];
            touched[min] = 1;
            pqlow.push(dad[min]);
        }
    }
    cout << "\nonlydad: \n";
    int last = 0;
    for (int j = 1; j <= sizedad; j++) {
        for (int i = sizedad; i >= 0; i--) {
            if (arrcount[i] == j && pqlow.top() != 1000) {
                if (last == -pqlow.top())
                    dad[i] = 0;
                else if (last == pqlow.top()) {
                    dad[i] = -pqlow.top();
                    last   = -pqlow.top();

                } else {
                    dad[i] = pqlow.top();
                    last   = pqlow.top();
                }
                cout << pqlow.top() << " ";
                pqlow.pop();
            }
        }
    }
    // for (int i = 0; i <= sizedad; i++) dad[i] = copydad[i];
}
void setuplen() {
    int k, x, j, t, i;
    for (k = 0; k <= 26; k++) // s.329
    {
        i = 0;
        x = 0;
        j = 1;
        if (arrcount[k])
            for (t = dad[k]; t; t = dad[t], j += j, i++)
                if (t < 0) {
                    x += j;
                    t = -t;
                }
        code[k] = x;
        len[k]  = i;
    }
}

int main() {
    for (int i = 0; i < 100; i++) arrcount[i] = 0;
    // string str = "IPSWICH MOT LEEDS SAMT WEST HAM MOT SWANSEA ER MORSOMT"; //h14_2 a
    string str =
        "SILDESALATEN SYNES SPESIELT SALT OG SYRLIG SIDEN DEN SMAKTE SLIK"; // s10_2a
    string sortstr = str;
    sort(sortstr.begin(), sortstr.end());
    sortstr.erase(unique(sortstr.begin(), sortstr.end()), sortstr.end());

    uniqueCount(arrcount, str.size(), str);
    printArray(sortstr, sortstr.size(), "");
    cout << endl;
    printArray(arrcount, str.size(), "count[k]:");
    cout << endl << endl;
    printArray(sortstr, sortstr.size(), "");
    cout << endl;
    printArray(arrcount, str.size(), "count[k]:");
    setupdad2(arrcount);
    cout << endl;
    printArray(dad, 27, "dad[k]:");
    cout << endl;
    cout << endl;

    int over[101];
    for (int i = 27; i <= 100; i++) over[i] = i;
    printArray(over, 26, 100, "k");
    cout << endl;
    printArray(arrcount, 26, 100, "count[k]:");
    cout << endl;
    printArray(dad, 26, 100, "dad[k]:");

    cout << endl;
    cout << endl << "tree Traverse TODO" << endl << endl << endl;
    printArray(sortstr, sortstr.size(), "");
    cout << endl;
    printArray(arrcount, 27, "count[k]:");
    setuplen();
    cout << endl;
    printArray(len, 27, "len[k]:");
    cout << endl;
    cout << "\t\t\t\t";
    for (int j = 0; j <= 27; j++) { // s.329
        for (int i = len[j]; i > 0; i--) cout << ((code[j] >> i - 1) & 1);
        cout << " ";
    }
    cout << endl;
    printArray(code, 27, "code[k]:");
    cout << endl << endl << endl;
    int result = 0;
    int shown  = 0;
    for (int j = 0; j <= 27; j++) {
        if (arrcount[j] * len[j] != 0) {
            shown++;
            result = (arrcount[j] * len[j]) + result;
            cout << '(' << arrcount[j] << '*' << len[j] << ')' << " + ";
            if (!(shown % 10)) cout << endl;
        }
    }
    cout << " = " << result << " bits";

    /*    cout << endl;
    for (int k = 0; k <= 26; k++) //s.329
      {
        int i = 0; int x = 0; int j = 1;
        if (arrcount[k])
          for (int t=dad[k]; t; t=dad[t], j+=j, i++)
            if (t < 0) { x += j; t = -t; }
        code[k] = x; len[k] = i;
        cout << x << " ";
      }*/

    //    printArray()
}

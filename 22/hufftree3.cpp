#include <iomanip>
#include <iostream>
#include <queue>
#include <string>
#include <unordered_map>
using namespace std;

int k[100];
int count[100];
int dad[100];
int code[100];

void printk(int a[], string str) {
    cout << str << setw(15 - str.size());
    for (int i = 0; i <= 28; i++) {
        if (a[i] != 0) cout << char(i + 64) << "\t";
    }
}
void printArray(int a[], int n, string str) {
    cout << str << setw(15 - str.size());
    for (int i = 0; i < n; i++) {
        if (a[i] != 0) cout << a[i] << "\t";
    }
}
void printArray(string a, int n, string str) {
    cout << str << setw(15 - str.size());
    for (int i = 0; i < n; i++) { cout << a[i] << "\t"; }
    cout << endl << 'K' << setw(15 - 1);
    for (int i = 0; i < n; i++) {
        if (a[i] == ' ')
            cout << 0 << "\t";

        else
            cout << (int)a[i] - 64 << "\t";
    }
}

// A Tree node
struct Node {
    char  ch;
    int   freq;
    Node *left, *right;
};

// Function to allocate a new tree node
Node* getNode(char ch, int freq, Node* left, Node* right) {
    Node* node = new Node();

    node->ch    = ch;
    node->freq  = freq;
    node->left  = left;
    node->right = right;

    return node;
}

// Comparison object to be used to order the heap
struct comp {
    bool operator()(Node* l, Node* r) {
        // highest priority item has lowest frequency
        return l->freq > r->freq;
    }
};

// traverse the Huffman Tree and store Huffman Codes
// in a map.
void encode(Node* root, string str, unordered_map<char, string>& huffmanCode) {
    if (root == nullptr) return;

    // found a leaf node
    if (!root->left && !root->right) { huffmanCode[root->ch] = str; }

    encode(root->left, str + "0", huffmanCode);
    encode(root->right, str + "1", huffmanCode);
}

// traverse the Huffman Tree and decode the encoded string
void decode(Node* root, int& index, string str) {
    if (root == nullptr) { return; }

    // found a leaf node
    if (!root->left && !root->right) {
        cout << root->ch;
        return;
    }

    index++;

    if (str[index] == '0')
        decode(root->left, index, str);
    else
        decode(root->right, index, str);
}

// Builds Huffman Tree and decode given input text
void buildHuffmanTree(string text) {
    // count frequency of appearance of each character
    // and store it in a map
    unordered_map<char, int> freq;
    for (char ch : text) {
        freq[ch]++;
        k[(int)ch - 64] = 1;
        if (ch == ' ')
            count[0]++;
        else
            count[(int)(ch)-64]++;
    }

    // Create a priority queue to store live nodes of
    // Huffman tree;
    priority_queue<Node*, vector<Node*>, comp> pq;

    // Create a leaf node for each character and add it
    // to the priority queue.
    for (auto pair : freq) {
        pq.push(getNode(pair.first, pair.second, nullptr, nullptr));
    }
    int i = 27;

    // do till there is more than one node in the queue
    while (pq.size() != 1) {
        // Remove the two nodes of highest priority
        // (lowest frequency) from the queue
        Node* left = pq.top();
        if (left->ch == ' ')
            dad[0] = i;
        else
            dad[(int)left->ch - 64] = i;
        cout << (int)left->ch -64 << "   awef";
        pq.pop();
        Node* right = pq.top();
        if (right->ch == ' ')
            dad[0] = -i;
        else
            dad[(int)right->ch - 64] = -i;
        pq.pop();

        // Create a new internal node with these two nodes
        // as children and with frequency equal to the sum
        // of the two nodes' frequencies. Add the new node
        // to the priority queue.
        int sum = left->freq + right->freq;
        pq.push(getNode('\0', sum, left, right));
        i++;
    }

    // root stores pointer to root of Huffman Tree
    Node* root = pq.top();

    // traverse the Huffman Tree and store Huffman Codes
    // in a map. Also prints them
    unordered_map<char, string> huffmanCode;
    encode(root, "", huffmanCode);

    cout << "Huffman Codes are :\n" << '\n';
    for (auto pair : huffmanCode) {
        cout << pair.first << " " << pair.second << '\n';
    }

    cout << "\nOriginal string was :\n" << text << '\n';

    // print encoded string
    string str       = "";
    int    countbits = 0;
    for (char ch : text) { str += huffmanCode[ch]; }
    countbits = str.length() + countbits;
    cout << endl << "bits: " << countbits;

    cout << "\nEncoded string is :\n" << str << '\n';

    // traverse the Huffman Tree again and this time
    // decode the encoded string
    int index = -1;
    cout << "\nDecoded string is: \n";
    while (index < (int)str.size() - 2) { decode(root, index, str); }

    cout << "\n\n\n";
    printk(k, "k\t ");
    cout << endl;
    printArray(count, 27, "count[k]:");
    cout << endl;
    printArray(dad, 27, "dad[k]:"); // TODO

    cout << "\n Treet \n\n\n";
    printk(k, "k\t ");
    cout << endl;
    printArray(count, 27, "count[k]:");
}

// Huffman coding algorithm
int main() {
    string text =
        "SILDESALATEN SYNES SPESIELT SALT OG SYRLIG SIDEN DEN SMAKTE SLIK";

    buildHuffmanTree(text);

    return 0;
}

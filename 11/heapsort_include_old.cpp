#include <iostream>

using namespace std;
int N = 0;
int heapArray[100];

void upheap(int k) {
    int v;
    v = heapArray[k];
    while (heapArray[k / 2] <= v) {
        heapArray[k] = heapArray[k / 2];
        k = k / 2;
    }
    heapArray[k] = v;
}

void insert(int item) {
    heapArray[++N] = item;
    upheap(N);
}
void remove() {
    int tmp = heapArray[N];
    heapArray[1] = heapArray[N];
    heapArray[N] = tmp;
    heapArray[N--] = 0;
    upheap(N);
}

int main() {
    heapArray[0] = 100;
/*    insert(4);   // D
    insert(9);   // I
    insert(18);  // R
    insert(5);   // E
    insert(19);  // S
    insert(20);  // T
    insert(18);  // R
    insert(1);   // A
    insert(9);   // I
    insert(20);  // T
    insert(19);  */// S
// 20 20 20 12 5 7 13 1 5
// T   T  T  L E G  M A E
/*insert(1);
insert(12);
insert(7);
insert(13);
insert(5);
insert(20);
insert(20);
insert(5);
insert(20); */

    insert(98);
    insert(87);
    insert(72);
    insert(49);
    insert(73);
    insert(70);
    insert(70);
    insert(45);
    insert(40);
    insert(46);
    insert(42);
    insert(31);
    insert(39);

    insert(87);
    insert(86);
    remove();
    for (int i = 1; i <= 17; i++) cout << heapArray[i] << " ";
}

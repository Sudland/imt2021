#include <iostream>
using namespace std;
int  mergeHelp[100 + 1]; // Hjelpearray for mergesort
void printArraylen(int arr[], int low, int high) {
    int i;
    for (i = 0; i < low; i++) { cout << "  "; }
    for (i = 0; i < high; i++) {
        if (low <= i && high >= i) printf("%c ", arr[i]);
    }
}

void mergeSort(int array[], int l, int r) { // SIDE 166 I LÆREBOKA:
    if (r > l) {                    // Hvis vi ikke har oversteget grensene
        int i, j;                   // Deklarer variabler vi trenger under
        int m = (r + l) / 2;        // Finn midten
        mergeSort(array, l, m);     // Mergesort venstre halvdel
        mergeSort(array, m + 1, r); // Mergesort høyre halvdel
                                    // Kopier over venstre halvdel:
        for (i = m + 1; i > l; i--) mergeHelp[i - 1] = array[i - 1];
        // Kopier over høyre halvdel baklengs:
        for (j = m; j < r; j++) mergeHelp[r + m - j] = array[j + 1];
        for (int k = l; k <= r; k++)
            array[k] = (mergeHelp[i] <= mergeHelp[j] && i <= m) ? mergeHelp[i++]
                                                                : mergeHelp[j--];
        printArraylen(array, l, r + 1);
        cout << "\n";
    }
}

int main() {
    //    int a[] = {'H', 'G', 'C', 'E', 'M', 'E', 'T', 'E' ,'R','Y'}; //HGCEMETERY h10 1c)
    int a[] = {'D', 'I', 'R', 'E', 'S', 'T',
               'R', 'A', 'I', 'T', 'S'}; // DIRESTRAITS h13 1c)
    int N   = (sizeof(a) / sizeof(a[0]));
    printArraylen(a, 0, N);
    cout << endl;
    mergeSort(a, 0, N - 1);
}

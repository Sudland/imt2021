//  Fil:  FRODEH \ ALG_MET \ EKSEMPEL \ EKS_10.CPP

//  Eksemplet viser koden fra side 166 i l�reboka (dvs. mergesort av array).
//  Versjon laget av:   Jonas Johan Solsvik

#include <iostream>
using namespace std;


const int LEN = 16;                       //  Tekstens lengde, inkl.'\0'.
char tekst[LEN] = "ASORTINGEXAMPLE";      //  Streng som skal sorteres.
char mergeArray[LEN];

typedef char itemType;

inline void displaySubarray(const itemType array[],
    const int left,
    const int right,
    const char message[]) {

    cout << message;
    for (int p = left; p <= right; p++) {
        cout << array[p] << ' ';
    }
}

//  Jfr. s.166 i l�reboka.

void mergesort(itemType array[], const int left, const int right) {

    int i, j, k;

    if (right > left) {
        const int middle = (right + left) / 2;

        mergesort(array, left, middle);
        mergesort(array, middle + 1, right);
        displaySubarray(array, left, right, "\n\nF�r:\t");

        //  Kopierer venstre del:

        for (i = middle + 1; i > left; i--) {
            mergeArray[i - 1] = array[i - 1];
        }

        //  Kopierer h�yre del BAKLENGS! :

        for (j = middle; j < right; j++) {
            mergeArray[right + middle - j] = array[j + 1];
        }

        displaySubarray(mergeArray, left, right, "\n\nInni:\t");

        //  Merge sammen delene inn i 'array':

        for (k = left; k <= right; k++) {
            if (mergeArray[i] <= mergeArray[j])
                array[k] = mergeArray[i++];
            else
                array[k] = mergeArray[j--];
        }

        displaySubarray(array, left, right, "\nEtter:\t");

        char ch;
        cin >> ch;
    }
}


int main(int argc, const char** argv) {
    char ch;
    cout << "\n\nViser bruken av MERGESORT p� array";   cin >> ch;
    mergesort(tekst, 0, LEN - 2);
    cout << "\n\nFerdig:  " << tekst << "\n\n";
    return 0;
}

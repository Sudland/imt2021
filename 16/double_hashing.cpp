#include <cstring>
#include <iostream>
const int M     = 17;
int       loopi = 0;
int       shash1[100];
int       shash2[100];

struct block {
    int key;
    int info;
};
block a[100];
void  printArraystr(const char arr[], int size) {
    int i;
    std::cout << "Keyene:\t\t";
    for (i = 0; i < size; i++) printf("%c\t", arr[i]);
    std::cout << "\n";
    std::cout << "K(alfa nr):\t";
    for (i = 0; i < size; i++) printf("%d\t", arr[i] - 64);
}
void printArray(block arr[], int size) {
    int i;
    for (i = 0; i < size; i++) printf("%d ", arr[i].key);
}
int  hash1(int k) { return (k % M); }
int  hash2(int k) { return (4 - (k % 4)); }
void insert(int v, int info) {
    int x           = hash1(v);
    shash1[loopi]   = x;
    int u           = hash2(v);
    shash2[loopi++] = u;
    int times       = 0;
    while (a[x].info != -1) {
        x = (x + u) % M;
        times++;
    };
    if (times <= 100) {
        char usage[3] = "";
        if (times) strcpy(usage, "*");
        std::cout << std::endl << x << " = " << usage << (char)(v + 64);
    }
    a[x].key  = v;
    a[x].info = info;
}
int main() {
    for (int i = 0; i < 100; i++) a[i].info = -1;
  //  const char arr[] = "ilikeit";
    const char arr[] = "ALGMETERSVETT";
    int        n     = sizeof(arr) / sizeof(arr[0]);
    printArraystr(arr, n);
    for (int i = 0; i < n - 1; i++) {
        int val = arr[i] - 64;
        insert(val, i);
    }
    std::cout << "\nhash1:";
    for (int i = 0; i < loopi; i++) printf("%d ", shash1[i]);
    std::cout << "\nhash2:";
    for (int i = 0; i < loopi; i++) printf("%d ", shash2[i]);
    std::cout << "\nindex:";
    printArray(a, n);
}

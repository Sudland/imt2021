#include <iostream>
using namespace std;

const int MAX_NODER = 101;
const int MAX_NABO  = 11;

struct node {
    int   navn;
    int   ant_kant;
    node* neste[MAX_NABO];
    node* kopi;
    node(int id, int ant) {
        navn     = id;
        ant_kant = ant;
        kopi     = NULL;
    }
};
int   V = 14;
node* G = new node(2, V);
node* GG;
node* noder[MAX_NODER];
void  koiper_1() {
    int i, j; //  † lage kopi-node, en gang for † peke til naboene.
    for (i = 1; i <= V; i++)
        // Lager kopi med likt navn og ant.naboer:
        //   (+10 KUN for † SE at kopi-node.)
        noder[i]->kopi = new node(noder[i]->navn + 10, noder[i]->ant_kant);

    for (i = 1; i <= V; i++) { //  G†r gjennom alle kopi-nodene,
        for (j = 1; j <= noder[i]->kopi->ant_kant; j++) //  og oppretter
            noder[i]->kopi->neste[j] = noder[i]->neste[j]->kopi; //  pekere til
    } //  alle naboene.
}

node* s;
void kopier_2(node*x){
  int j;
     if (x->kopi == NULL)  {              // Noden er enn† ikke kopiert:
                                          // +20 KUN for † SE at kopi-node:
        x->kopi = new node(x->navn+20, x->ant_kant);

        for (j = 1;  j <= x->ant_kant;  j++)  {
            kopier_2(x->neste[j]);
            x->kopi->neste[j] = x->neste[j]->kopi;
        }
     }
}


int main() {}

// C++ program to print DFS traversal from
// a given vertex in a  given graph
#include<bits/stdc++.h>
using namespace std;

// Graph class represents a directed graph
// using adjacency list representation
class Graph
{
    int V;    // No. of vertices

    // Pointer to an array containing
    // adjacency lists
    list<int> *adj;

    // A recursive function used by DFS
    void DFSUtil(int v, bool visited[]);
public:
    Graph(int V);   // Constructor

    // function to add an edge to graph
    void addEdge(int v, int w);

    // DFS traversal of the vertices
    // reachable from v
    void DFS(int v);
};

Graph::Graph(int V)
{
    this->V = V;
    adj = new list<int>[V];
}

void Graph::addEdge(int v, int w)
{
    adj[v].push_back(w); // Add w to v’s list.
}

void Graph::DFSUtil(int v, bool visited[])
{
    // Mark the current node as visited and
    // print it
    visited[v] = true;
    printf("%c ",(char)v+65);

    // Recur for all the vertices adjacent
    // to this vertex
    list<int>::iterator i;
    for (i = adj[v].begin(); i != adj[v].end(); ++i)
            DFSUtil(*i, visited);
}

// DFS traversal of the vertices reachable from v.
// It uses recursive DFSUtil()
void Graph::DFS(int v)
{
    // Mark all the vertices as not visited
    bool *visited = new bool[V];
    for (int i = 0; i < V; i++)
        visited[i] = false;

    // Call the recursive helper function
    // to print DFS traversal
    DFSUtil(v, visited);
}

// Driver code
int main()
{ //
    // Create a graph given in the above diagram
    Graph g(8);
    g.addEdge((int)'A'-65, (int)'B'-65);
    g.addEdge((int)'A'-65, (int)'F'-65);

    g.addEdge((int)'B'-65, (int)'C'-65);
    g.addEdge((int)'B'-65, (int)'G'-65);

    g.addEdge((int)'C'-65, (int)'G'-65);

    g.addEdge((int)'D'-65, (int)'E'-65);
    g.addEdge((int)'D'-65, (int)'F'-65);

    g.addEdge((int)'F'-65, (int)'G'-65);

    cout << "Following is Depth First Traversal"
            " (starting from vertex 2) \n";
    g.DFS((int)'D'-65);

    return 0;
}

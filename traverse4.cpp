traverse(struct node* t) // s.60 traverse inorder
{
    if (t != z) {
        traverse(t->l);
        visit(t);
        traverse(t->r);
    }
}

visit(struct node* t) {
    t->x = ++x;
    t->y = y;
}

traverse(struct node* t) // Inorder s.61
{
    y++;
    if (t != z) {
        traverse(t->l);
        visit(t);
        traverse(t->r)
    }
    y--;
}

traverse(struct node* t) // Preorder
{
    if (t != z) {
        visit(t);
        traverse(t->l);
        traverse(t->r);
    }
}
traverse(struct node* t) // Postorder
{
    if (t != z) {
        traverse(t->l);
        traverse(t->r);
        visit(t);
    }
}

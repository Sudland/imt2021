
const int ANTX = 9, ANTY = 9, STRLEN = 80, TOTSUM = 45;
int       ant;
int       s[ANTX][ANTY];

void finn_losning(int n) {
    int tot = ANTX * ANTY; 
    int i, j, k, ii, jj, tall, dx1, dx2, dy1, dy2;

    if (s[n / 9][n % 9] == 0) { // Sjekk om cellen er tom
        int brukt[ANTX + 1] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
        for (jj = 0; jj < ANTY; jj++) {
            tall = s[n / 9][jj];
            if (tall && !brukt[tall]) {
                brukt[tall] = 1;
            }
        }
        for (ii = 0; ii < ANTX; ii++) {
            tall = s[n / 9][n % 9];
            if (tall && !brukt[tall]) {
                brukt[tall] = 1;
            }
        }
        ii = (n / 9) % 3;
        jj = (n % 9) % 3;
        switch (ii) {
            case 0:
                dx1 = 1;
                dx2 = 2;
                break;
            case 1:
                dx1 = -1;
                dx2 = 1;
                break;
            case 2:
                dx1 = -2;
                dx2 = -1;
                break;
        }
        switch (jj) {
            case 0:
                dy1 = 1;
                dy2 = 2;
                break;
            case 1:
                dy1 = -1;
                dy2 = 1;
                break;
            case 2:
                dy1 = -2;
                dy2 = -1;
                break;
        }
        for (k = 1; k <= ANTX; k++) {
            switch (k) {
                case 1:
                    ii = i;
                    jj = j;
                    break;
                case 2:
                    ii = i + dx1;
                    jj = j;
                    break;
                case 3:
                    ii = i + dx2;
                    jj = j;
                    break;
                case 4:
                    ii = i;
                    jj = j + dy1;
                    break;
                case 5:
                    ii = i + dx1;
                    jj = j + dy1;
                    break;
                case 6:
                    ii = i + dx2;
                    jj = j + dy1;
                    break;
                case 7:
                    ii = i;
                    jj = j + dy2;
                    break;
                case 8:
                    ii = i + dx1;
                    jj = j + dy2;
                    break;
                case 9:
                    ii = i + dx2;
                    jj = j + dy2;
                    break;
            }
            tall = s[ii][jj];
            if (tall && !brukt[tall]) 
                brukt[tall] = 1;
        }
            
            for (k = 1; k <= ANTX; k++)  //  Går gjennom ALLE tallene:
                if (!brukt[k]) {         //  Ikke brukt/prøvd:
                    s[i][j] = k;         //  Prøver med dette tallet.
                    finn_losning(n + 1); //  Neste rute fylles (om mulig).
                    s[i][j] = 0;         //  Tar vekk igjen prøvet tall.
                }
    } else
        finn_losning(n + 1);
}

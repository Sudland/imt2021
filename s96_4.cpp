#include <iostream>

using namespace std;

const int N = 6; //  Antall noder i grafen

int G[N + 1][N + 1] = {{0, 0, 0, 0, 0, 0, 0},  //  Nabomatrisen (laget ut
                       {0, 1, 1, 0, 1, 0, 0},  //  fra figuren i oppgaven)
                       {0, 1, 1, 1, 1, 1, 1},  //
                       {0, 0, 1, 1, 0, 0, 0},  //
                       {0, 1, 1, 0, 1, 0, 1},  //
                       {0, 0, 1, 0, 0, 1, 0},  //
                       {0, 0, 1, 0, 1, 0, 1}}; //

int utplukk[N + 1]; //  Ikke-nabo utplukk.
int ant = 0;        //  Antallet i utplukket.

void genresten(int k) {
    bool ok;
    int  i, j, m;
    for (i = k; i <= N; i++) { // Fors›k med ALLE en-nodes utvidelser:
        ok = true;
        for (j = 1; j <= ant; j++)
            if (G[utplukk[j]][i]) {
                ok = false;
                break;
            }                   // Ubrukelig pga. kant.
        if (ok) {               //  'i' er en brukbar utvidelse:
            utplukk[++ant] = i; //  Utskrift:
            cout << '\n';
            for (m = 1; m <= ant; m++) cout << utplukk[m];
            if (i < N) genresten(i + 1); //  Genererer mere ?
            ant--;
        }
    }
}

int main() {
    genresten(1);
    return 0;
}

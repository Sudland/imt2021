#include <iostream>
using namespace std;
char victim[3];
bool pathc = 0;
bool wb    = 0;
class EQ // s.444
{
private:
    int* olddad;
    int* dad;
    int* imA;
    int  N;
    bool weigth;
    bool changed;
    bool changedpathc;

public:
    EQ(int size);
    int  find(int x, int y, int doit);
    void display();
};

EQ::EQ(int size) {
    N      = size;
    dad    = new int[size];
    olddad = new int[size];
    imA    = new int[size];
}

int EQ::find(int x, int y, int doit) // Path Compression and weight balancing s.447
{
    changed      = 0;
    weigth       = 0;
    changedpathc = 0;
    for (int i = 0; i < N; i++) olddad[i] = dad[i];

    victim[0] = char(x + 64);
    victim[1] = char(y + 64);
    int t, i = x, j = y;
    while (dad[i] > 0) {
        i      = dad[i];
        imA[i] = 1;
    }
    while (dad[j] > 0) {
        j      = dad[j];
        imA[j] = 1;
    }
    while (dad[x] > 0) {
        t = x;
        x = dad[x];
        if (dad[t] < 0 || pathc) {
            dad[t]  = i;
            changed = 1;
        }
        imA[t] = 1;
    }
    while (dad[y] > 0) {
        t = y;
        y = dad[y];
        if (dad[t] < 0 || pathc) {
            dad[t]  = j;
            changed = 1;
        }
        imA[t] = 1;
    }
    if (doit && (i != j))
        if (dad[j] < dad[i] && wb) {
            dad[j] += dad[i] - 1;
            changed = 1;
            dad[i]  = j;
            imA[i]  = 1;
            weigth  = 1;
        } else if (wb) {
            if (dad[i] < dad[j]) weigth = 1;
            dad[i] += dad[j] - 1;
            changed = 1;
            dad[j]  = i;
            imA[j]  = 1;
        } else if (!wb && pathc)
            { dad[j] += dad[i]-1; dad[i] = j; changedpathc=1;}
    this->display();
    return (i != j);
}

void EQ::display() {
    for (int i = 1; i < N; i++) {
        if (olddad[i] != dad[i] && dad[i] > 0 && olddad[i] > 0)
            changedpathc = 1;
    }
    cout << endl << victim << ":\t";
    for (int i = 1; i < N; i++) {
        if (dad[i] < 0)
            cout << -dad[i] << "\t";
        else if (dad[i] == 0 && imA[i])
            cout << (char)(dad[i] + 64) << "\t";
        else if (dad[i] == 0)
            cout << '-' << "\t";
        else
            cout << (char)(dad[i] + 64) << "\t";
    }
    if (changedpathc && pathc)
        cout << "\t\tPath Compression";
    else if (!changed)
        cout << "\t\t(Ingenting skjer)";
    else if (weigth)
        cout << "\t\tWeigth Balancing";
    changed = 0;
}
int main() {
    // abcdefg
    // 1234567

    // s10 2b) w/weigth balancing, ikke path compression
    // pathc = 0; // enable Path Compression
    // wb = 1; // enable weigth compression
    /*EQ eq(8);
    eq.find('A' - 64, 'E' - 64, 1); // AE
    eq.find('B' - 64, 'C' - 64, 1); // BC
    eq.find('D' - 64, 'F' - 64, 1); // DF
    eq.find('G' - 64, 'A' - 64, 1); // GA
    eq.find('D' - 64, 'B' - 64, 1); // DB
    eq.find('C' - 64, 'E' - 64, 1); // CE
    eq.find('F' - 64, 'E' - 64, 1); // FE
    eq.find('B' - 64, 'F' - 64, 1); // BF
    eq.find('D' - 64, 'G' - 64, 1); // DG
    */

    // s16 2a) weigth balancing og path compression
    /*    pathc = 1; // enable Path Compression
        //wb = 1; // enable weigth compression
        EQ eq(7);
        eq.find('F' - 64, 'C' - 64, 1); // FC
        eq.find('D' - 64, 'A' - 64, 1); // DA
        eq.find('E' - 64, 'F' - 64, 1); // EF
        eq.find('A' - 64, 'E' - 64, 1); // AE
        eq.find('C' - 64, 'A' - 64, 1); // CA
        eq.find('B' - 64, 'F' - 64, 1); // BF
    */

    // s01 2a) ikke weigth balancing, men path compression
/*    pathc = 1; // enable Path Compression
    wb    = 0; // enable weigth compression
    EQ eq(7);
    eq.find('B' - 64, 'C' - 64, 1); // BC
    eq.find('A' - 64, 'E' - 64, 1); // AE
    eq.find('D' - 64, 'B' - 64, 1); // DB
    eq.find('B' - 64, 'F' - 64, 1); // BF
    eq.find('E' - 64, 'C' - 64, 1); // EC
    */


    // h18 2b) weigth balancing og path compression
    /*pathc = 1; // enable Path Compression
    wb    = 1; // enable weigth compression
    EQ eq(7);
    eq.find('F' - 64, 'A' - 64, 1); // FA
    eq.find('E' - 64, 'C' - 64, 1); // EC
    eq.find('B' - 64, 'D' - 64, 1); // BD
    eq.find('D' - 64, 'A' - 64, 1); // DA
    eq.find('E' - 64, 'F' - 64, 1); // EF
    eq.find('D' - 64, 'C' - 64, 1); // DC
    eq.find('C' - 64, 'F' - 64, 1); // CF
    */


    // h18 2b) weigth balancing og path compression
    /*pathc = 1; // enable Path Compression
    wb    = 1; // enable weigth compression
    EQ eq(7);
    eq.find('F' - 64, 'D' - 64, 1); // FD
    eq.find('B' - 64, 'A' - 64, 1); // BA
    eq.find('E' - 64, 'C' - 64, 1); // EC
    eq.find('C' - 64, 'D' - 64, 1); // CD
    eq.find('A' - 64, 'F' - 64, 1); // AF
    eq.find('C' - 64, 'A' - 64, 1); // CA
    */
    pathc = 1; // enable Path Compression
    wb    = 0; // enable weigth compression
    EQ eq(6);
    eq.find('A' - 64, 'E' - 64, 1); // AE
    eq.find('B' - 64, 'A' - 64, 1); // BA
    eq.find('C' - 64, 'A' - 64, 1); // CA
    eq.find('E' - 64, 'D' - 64, 1); // ED
    eq.find('C' - 64, 'E' - 64, 1); // CE
}
